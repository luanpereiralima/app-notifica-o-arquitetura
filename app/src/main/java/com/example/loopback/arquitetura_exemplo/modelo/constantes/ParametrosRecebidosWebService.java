package com.example.loopback.arquitetura_exemplo.modelo.constantes;

/**
 * Created by loopback on 04/11/15.
 */
public class ParametrosRecebidosWebService {
    public static final int CODIGO_SUCESSO = 200;
    public static final int FALHA_NOS_PARAMETROS = -8;
    public static final int FALHA_INTERNA = -10;
    public static final int NAO_HA_RESULTADOS = -1;
}
