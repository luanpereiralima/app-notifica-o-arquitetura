/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.loopback.arquitetura_exemplo.visao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.loopback.arquitetura_exemplo.R;
import com.example.loopback.arquitetura_exemplo.modelo.ServiceBuscarDados;
import com.example.loopback.arquitetura_exemplo.modelo.constantes.ConstantesConfig;

public class MainActivity extends AppCompatActivity {

    private BroadcastReceiver mMessageReceived;
    private TextView mLastMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLastMessage = (TextView) findViewById(R.id.lastMessage);

        mMessageReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mLastMessage.setText("Ultima mensagem: \n"+intent.getExtras().getString("mensagem"));
            }
        };
        startService(new Intent(this, ServiceBuscarDados.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceived,
                new IntentFilter(ConstantesConfig.RECEIVED_MESSAGE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceived);
        super.onPause();
    }
}
