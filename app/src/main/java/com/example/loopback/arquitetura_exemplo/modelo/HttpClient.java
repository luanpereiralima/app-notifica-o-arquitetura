package com.example.loopback.arquitetura_exemplo.modelo;

import android.util.Log;

import com.example.loopback.arquitetura_exemplo.modelo.constantes.ParametrosRecebidosWebService;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaInternaException;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaParametrosException;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.IOException;

/**
 * Created by Luan on 27/08/2015.
 */

public class HttpClient {
    private OkHttpClient client;

    public HttpClient(){
        this.client = new OkHttpClient();
    }

    public String JSONReceived(String url, RequestBody params) throws FalhaParametrosException, FalhaInternaException {

        String userAgent = System.getProperty("http.agent");

        //TODO CERTIFICAÇÂO PARA HTTPS https://github.com/square/okhttp/wiki/HTTPS
        /*client.setCertificatePinner(
                new CertificatePinner.Builder()
                .add("host", "sha1/blabla")
                .build()
        );*/

        if(url==null)
            throw new FalhaParametrosException("URL passada para o metodo e nula.");

        Request.Builder builder = new Request.Builder()
            .url(url)
            .addHeader("User-Agent", userAgent);

        if (params != null)
            builder = builder.post(params);

        Request request = builder.build();

        Response resposta;
        try {
            resposta = client.newCall(request).execute();
            if(resposta!=null) {
                int codeResp = resposta.code();
                if(codeResp != ParametrosRecebidosWebService.CODIGO_SUCESSO) {
                    Log.i("teste", "Code Received: " + codeResp);
                    throw new FalhaInternaException("CODE SERVER: "+ codeResp);
                }

            }
        } catch (IOException e) {
            throw new FalhaInternaException(e);
        }

        if (resposta.isSuccessful()) {
            String retorno;

            try {
                retorno = resposta.body().string().trim();
                Log.i("teste", "O retorno foi : "+retorno);
            } catch (IOException e) {
                throw new FalhaInternaException(e);
            }

            try {

                int numeroRetornado = Integer.parseInt(retorno);
                Log.i("teste", numeroRetornado+"");

                if (numeroRetornado == ParametrosRecebidosWebService.FALHA_NOS_PARAMETROS)
                    throw new FalhaParametrosException("FALHA NOS PARAMETROS PARA O SERVIDOR");
                else if (numeroRetornado == ParametrosRecebidosWebService.FALHA_INTERNA)
                    throw new FalhaInternaException("FALHA INTERNA NO SERVIDOR");
                else if(numeroRetornado == ParametrosRecebidosWebService.NAO_HA_RESULTADOS) {
                    Log.i("teste", "NÃO EXISTEM NOVAS MENSAGENS!");
                    return null;
                }
                else
                    return retorno;

            }catch (NumberFormatException e) {
                return retorno;
            }
        }
        throw new FalhaInternaException("sem sucesso!");

    }
}
