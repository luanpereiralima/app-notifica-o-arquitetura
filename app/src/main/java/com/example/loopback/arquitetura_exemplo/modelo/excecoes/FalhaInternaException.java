package com.example.loopback.arquitetura_exemplo.modelo.excecoes;

/**
 * Created by loopback on 04/11/15.
 */
public class FalhaInternaException extends Exception{

    public FalhaInternaException(String falha){
        super(falha);
    }

    public FalhaInternaException(Exception e){
        super(e);
    }
}
