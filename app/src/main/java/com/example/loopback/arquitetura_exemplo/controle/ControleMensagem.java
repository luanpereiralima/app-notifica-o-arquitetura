package com.example.loopback.arquitetura_exemplo.controle;

import com.example.loopback.arquitetura_exemplo.modelo.HttpClient;
import com.example.loopback.arquitetura_exemplo.modelo.Mensagem;
import com.example.loopback.arquitetura_exemplo.modelo.MMensagem;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaInternaException;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaParametrosException;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import java.util.List;

/**
 * Created by Luan on 27/08/2015.
 */

public class ControleMensagem {
    private MMensagem mMMensagem;
    private static final String METODO_BUSCAR = "buscar";

    public ControleMensagem(){
        mMMensagem = new MMensagem(new HttpClient());
    }

    public List<Mensagem> buscarMensagens(Long ultimaDaLista) throws FalhaParametrosException, FalhaInternaException {
        if(ultimaDaLista==null)
            throw new FalhaParametrosException("ULTIMO DA LISTA NÃO PODE SER NULO");

            RequestBody params = new FormEncodingBuilder()
                    .add("data", ultimaDaLista+"")
                    .add("metodo", METODO_BUSCAR)
                    .build();
        return mMMensagem.buscar(params);
    }
}
