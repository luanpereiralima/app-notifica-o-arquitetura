package com.example.loopback.arquitetura_exemplo.modelo.excecoes;

/**
 * Created by loopback on 04/11/15.
 */
public class FalhaParametrosException extends Exception{
    public FalhaParametrosException(String falha){
        super(falha);
    }
}
