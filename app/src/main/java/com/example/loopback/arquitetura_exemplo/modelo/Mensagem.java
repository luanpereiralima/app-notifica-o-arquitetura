package com.example.loopback.arquitetura_exemplo.modelo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by loopback on 04/11/15.
 */
public class Mensagem {
    private String mensagem;
    private long data;

    public Mensagem(String mensagem, long data){
        this.mensagem = mensagem;
        this.data = data;
    }

    public long getData() {
        return data;
    }

    public String getMensagem() {
        return mensagem;
    }

    public static final String
        mensagemTag = "mensagem",
        dataTag = "data";

    public static Mensagem getMensagemDoJSON(JSONObject json) throws JSONException{
        Mensagem mensagem = new Mensagem(json.getString(mensagemTag), json.getLong(dataTag));
        return mensagem;
    }
}
