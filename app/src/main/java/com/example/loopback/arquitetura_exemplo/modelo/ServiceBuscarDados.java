package com.example.loopback.arquitetura_exemplo.modelo;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.example.loopback.arquitetura_exemplo.controle.ControleMensagem;
import com.example.loopback.arquitetura_exemplo.modelo.constantes.ConstantesConfig;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaInternaException;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaParametrosException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by loopback on 04/11/15.
 */

public class ServiceBuscarDados extends Service {
    long dataUltimaMensagem = 1l;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("teste", "teste");
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 10000);
    }

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            try {
                List<Mensagem> lista = new ControleMensagem().buscarMensagens(dataUltimaMensagem);

                if(lista!=null){
                    Intent messageReceived = new Intent(ConstantesConfig.RECEIVED_MESSAGE);
                    Mensagem recebida = lista.get(lista.size()-1);
                    messageReceived.putExtra("mensagem", recebida.getMensagem());
                    dataUltimaMensagem = recebida.getData();
                    LocalBroadcastManager.getInstance(ServiceBuscarDados.this).sendBroadcast(messageReceived);
                }

            } catch (FalhaParametrosException e) {
                e.printStackTrace();
            } catch (FalhaInternaException e) {
                e.printStackTrace();
            }

        }
    };
}
