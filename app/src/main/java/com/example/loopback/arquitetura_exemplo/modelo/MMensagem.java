package com.example.loopback.arquitetura_exemplo.modelo;

import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaInternaException;
import com.example.loopback.arquitetura_exemplo.modelo.excecoes.FalhaParametrosException;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luan on 25/08/2015.
 */

public class MMensagem {
    private HttpClient client;
    private String URL_MENSAGEM = "http://10.0.3.2/arquitetura/mensagens.php";

    public MMensagem(HttpClient client){
        this.client = client;
    }

    public List<Mensagem> buscar(RequestBody params) throws FalhaParametrosException, FalhaInternaException{

            String resposta = client.JSONReceived(URL_MENSAGEM, params);
            if(resposta==null)
                return null;

            try {
                JSONArray arrayMensagens = new JSONArray(resposta);
                List<Mensagem> listaMensagens = new ArrayList();

                for (int i = 0; i < arrayMensagens.length(); i++)
                    listaMensagens.add(Mensagem.getMensagemDoJSON(arrayMensagens.getJSONObject(i)));

                return listaMensagens;
            } catch (JSONException ee) {
                ee.printStackTrace();
            }

        return null;
    }
}
